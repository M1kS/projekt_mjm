﻿using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace ASPNetCoreIdentityCustomFields.Data
{
    public class ApplicationUser : IdentityUser
    {
        public bool admin { get; set; }
        public string? firstName { get; set; }
        public string? lastName { get; set; }

        public static explicit operator ApplicationUser(ClaimsPrincipal v)
        {
            throw new NotImplementedException();
        }
    }
}