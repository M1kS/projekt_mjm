﻿using Microsoft.EntityFrameworkCore;

namespace WebApplication2.Models
{
    public class Offer
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Img_url { get; set; }
        public decimal Price { get; set; }


        public Offer()
        {

        }
    }

}